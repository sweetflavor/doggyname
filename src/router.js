import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'
import Generator from './views/Generator.vue'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/generator',
      name: 'generator',
      component: Generator
    }
  ]
})
